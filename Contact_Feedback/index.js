var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Details = require('./app/models/details');
var MongoClient = require('mongodb').MongoClient;

// Configure app for bodyParser()
// lets us grab data from the body of POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Set up port for server to listen on
var port = process.env.PORT || 3002;

// Connect to DB
var url = 'mongodb://localhost:27017/';

MongoClient.connect(url, (err, db) => {
    if (err) throw err;
    const dbo = db.db("UFC");

    //API Routes
    var router = express.Router();

    // Routes will all be prefixed with /API
    app.use('/api', router);

    //MIDDLE WARE-
    router.use(function (req, res, next) {
        console.log('FYI...There is some processing currently going down');
        next();
    });

    // test route
    router.get('/', function (req, res) {
        res.json({
            message: 'Welcome !'
        });
    });

    router.route('/contact')
        .post(function (req, res) {
            var person = new Details();
            person.userName = req.body.userName;
            person.userEmail = req.body.userEmail;
            person.givenFeedback = req.body.givenFeedback;

            dbo.collection("feedback").insertOne(person, function (err, rese) {
                if (err) throw {
                    message: "Error comes " + err.message,
                };
                console.log("1 document inserted sucessfully");
                res.send("Document successfully inserted");
            });
        })
        .get(function (req, res) {
            dbo.collection("feedback").find({}).toArray(function (err, result) {
                if (err) throw err;
                res.send(result);
            });
        });
});
// Fire up server
app.listen(port);

// print friendly message to console
console.log('Server listening on port ' + port);