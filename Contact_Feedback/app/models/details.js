var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DetailSchema = new Schema({
    userName: String,
    userEmail: String,
    givenFeedback: String,
});

module.exports = mongoose.model('Details', DetailSchema);