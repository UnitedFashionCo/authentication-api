# UFC-Backend

<table>
  <tr>
    <th>Module Name</th>
    <th>Database Name</th>
    <th>Collection Name</th>
    <th>Route</th>
    <th>Post</th>
    <th>Get</th>
    <th>Description</th>
  </tr>
 
 
  <tr>
  <td>Authentication</td>
    <td> UFC </td>
    <td> details </td>
    <td> http://localhost:3001/api/details/ </td>
    <td>Yes</td>
    <td>No</td>
    <td>SignUp</td>
  </tr>
 
  <tr>
  <td>Authentication</td>
    <td> UFC </td>
    <td> details </td>
    <td> http://localhost:3001/api/details/ </td>
    <td>No</td>
    <td>Yes</td>
    <td>login</td>
  </tr>
 
 
  <tr>
  <td>Contact_Feedback</td>
    <td> UFC </td>
    <td> feedback </td>
    <td> http://localhost:3002/api/contact/ </td>
    <td>Yes</td>
    <td>Yes</td>
    <td>Feedback and get all Feedback</td>
  </tr>
  </table>
